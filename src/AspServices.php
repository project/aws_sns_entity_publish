<?php

namespace Drupal\aws_sns_entity_publish;

use Drupal\Core\Config\ConfigFactoryInterface;
use Aws\Sns\SnsClient;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\Crypt;

/**
 * Class AspServices to define Utility functions.
 *
 * @package Drupal\aws_sns_entity_publish\Services
 */
class AspServices {

  /**
   * The ConfigFactory for getting SNS configurations.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Constructs a new MostPopularArticle object.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory for accessing SNS configurations.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Function to push to AWS SNS.
   */
  public function entityPublishToAwsSns($subject, $message, $topic = '', $message_group_id = '', $message_dup_id = '') {
    $config = $this->configFactory->get('aws_sns_entity_publish.awsconfig');

    $params = [
      'credentials' => [
        'key' => $config->get('aws_sns_key'),
        'secret' => $config->get('aws_sns_secret'),
      ],
      'region' => $config->get('aws_sns_region'),
      'version' => $config->get('aws_sns_version'),
    ];
    $client = new SnsClient($params);

    $result = $client->publish([
      'TopicArn' => ($topic !== '') ? $topic : $config->get('aws_sns_default_topic_arn'),
      // Message is required.
      'Message' => $message,
      'MessageGroupId' => ($message_group_id !== '') ? $message_group_id : Crypt::randomBytesBase64(8),
      'MessageDeduplicationId' => ($message_dup_id !== '') ? $message_dup_id : Crypt::randomBytesBase64(8),
      'Subject' => $subject,
      'MessageStructure' => 'aws sns notify',
    ]);

    $message_id = $result->search('MessageId');
    $this->messenger()->addStatus($this->t('AWS SNS message published. Msg id: @value', ['@value' => $message_id]));
    return $message_id;
  }

  /**
   * Convert a multi-dimensional array into a single-dimensional array.
   *
   * @param array $array
   *   The multi-dimensional array.
   *
   * @return array
   *   Array list.
   */
  public function flattenArray(array $array) {
    if (!is_array($array)) {
      return FALSE;
    }
    $result = [];
    foreach ($array as $value) {
      $result[$value['TopicArn']] = $value['TopicArn'];
    }
    return $result;
  }

  /**
   * Fetch topic list from aws sns.
   *
   * @return array
   *   Array list of topics.
   */
  public function listTopic() {
    $config = $this->configFactory->get('aws_sns_entity_publish.awsconfig');
    $params = [
      'credentials' => [
        'key' => $config->get('aws_sns_key'),
        'secret' => $config->get('aws_sns_secret'),
      ],
      'region' => $config->get('aws_sns_region'),
      'version' => $config->get('aws_sns_version'),
    ];
    $topics = [];
    if (!empty($config->get('aws_sns_key')) && !empty($config->get('aws_sns_secret'))) {
      $client = new SnsClient($params);
      $result = $client->listTopics([]);
      $topics = $this->flattenArray($result->search('Topics'));
    }
    return $topics;
  }

  /**
   * Fetch entity list from aws sns.
   *
   * @return array
   *   Array list of an entities.
   */
  public function listEntities() {
    $listed_entities = '';
    $config = $this->configFactory->get('aws_sns_entity_publish.awsconfig');
    $aws_sns_mapping_list = $config->get('aws_sns_custom_event_attributes');
    if (!empty($aws_sns_mapping_list)) {
      $listed_entities = explode(",", $aws_sns_mapping_list);
    }
    return $listed_entities;

  }

}

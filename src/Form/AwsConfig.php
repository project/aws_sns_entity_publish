<?php

namespace Drupal\aws_sns_entity_publish\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AwsConfig is a configuration form for AWS connection.
 */
class AwsConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_sns_entity_publish.awsconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('aws_sns_entity_publish.awsconfig');
    $form['aws_sns_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS SNS Key'),
      '#description' => $this->t('AWS SNS key for your account.'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('aws_sns_key'),
    ];
    $form['aws_sns_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS SNS Secret'),
      '#description' => $this->t('AWS SNS Secret for your account.'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('aws_sns_secret'),
    ];
    $form['aws_sns_region'] = [
      '#type' => 'select',
      '#title' => $this->t('AWS SNS Queue Region'),
      '#default_value' => $config->get('aws_sns_region'),
      '#options' => [
        'us-east-1' => $this->t('US East (N. Virginia)'),
        'us-east-2' => $this->t('US East (Ohio)'),
        'us-west-1' => $this->t('US West (N. California)'),
        'us-west-2' => $this->t('US West (Oregon)'),
        'ap-southeast-1' => $this->t('Asia Pacific (Singapore)'),
        'ap-northeast-1' => $this->t('Asia Pacific (Tokyo)'),
        'sa-east-1' => $this->t('South America (São Paulo)'),
        'af-south-1' => $this->t('Africa (Cape Town)'),
        'ap-east-1' => $this->t('Asia Pacific (Hong Kong)'),
        'ap-south-1' => $this->t('Asia Pacific (Mumbai)'),
        'ap-northeast-3' => $this->t('Asia Pacific (Osaka-Local)'),
        'ap-northeast-2' => $this->t('Asia Pacific (Seoul)'),
        'ap-southeast-2' => $this->t('Asia Pacific (Sydney)'),
        'ca-central-1' => $this->t('Canada (Central)'),
        'cn-north-1' => $this->t('China (Beijing)'),
        'cn-northwest-1' => $this->t('China (Ningxia)'),
        'eu-central-1' => $this->t('Europe (Frankfurt)'),
        'eu-west-1' => $this->t('Europe (Ireland)'),
        'eu-west-2' => $this->t('Europe (London)'),
        'eu-west-3' => $this->t('Europe (Paris)'),
        'eu-south-1' => $this->t('Europe (Milan)'),
        'eu-north-1' => $this->t('Europe (Stockholm)'),
        'me-south-1' => $this->t('Middle East (Bahrain)'),
        'us-gov-east-1' => $this->t('AWS GovCloud (US-East)'),
        'us-gov-west-1' => $this->t('AWS GovCloud (US)'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('AWS SNS Region where to store the Queue. The list of AWS Regions can be found <a href="@aws_regions_list" target="_blank">here</a>', [
        '@aws_regions_list' => 'https://docs.aws.amazon.com/general/latest/gr/sqs-service.html',
      ]),
    ];

    $form['aws_sns_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#default_value' => $config->get('aws_sns_version'),
      '#required' => TRUE,
      '#description' => $this->t("Amazon Web Services Version. 'latest' recommended"),
    ];
    $form['aws_sns_default_topic_arn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS SNS Default Topic Arn'),
      '#description' => $this->t('Default AWS SNS topic arn. Create topic by visiting AWS account (https://us-east-2.console.aws.amazon.com/sns/v3/home?region=us-east-2#/create-topic)'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('aws_sns_default_topic_arn'),
    ];
    $form['aws_sns_custom_event_attributes'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Custom Event Attributes"),
      '#required' => FALSE,
      '#description' => $this->t('Please enter the custom event attributes in comma seperated values. Post that you can able to do the AWS SNS Topic Mapping in Mapping Form. Example : Event1, Event2, Event3'),
      '#cols' => 60,
      '#rows' => 5,
      '#attributes' => [
        'placeholder' => $this->t("Example : Event1, Event2, Event3"),
      ],
      '#default_value' => $config->get('aws_sns_custom_event_attributes'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('aws_sns_entity_publish.awsconfig')
      ->set('aws_sns_key', $form_state->getValue('aws_sns_key'))
      ->set('aws_sns_secret', $form_state->getValue('aws_sns_secret'))
      ->set('aws_sns_default_topic_arn', $form_state->getValue('aws_sns_default_topic_arn'))
      ->set('aws_sns_region', $form_state->getValue('aws_sns_region'))
      ->set('aws_sns_version', $form_state->getValue('aws_sns_version'))
      ->set('aws_sns_custom_event_attributes', $form_state->getValue('aws_sns_custom_event_attributes'))
      ->save();
  }

}

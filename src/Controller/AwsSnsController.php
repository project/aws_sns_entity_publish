<?php

namespace Drupal\aws_sns_entity_publish\Controller;

use Drupal\aws_sns_entity_publish\AspServices;
use Drupal\Core\Controller\ControllerBase;
use Drupal\system\SystemManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AwsSnsController.
 *
 * Handles test connection link and publish a test message to topic.
 */
class AwsSnsController extends ControllerBase {

  /**
   * System Manager Service.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * Asp Service.
   *
   * @var \Drupal\aws_sns_entity_publish\AspServices
   */
  protected $aspServices;

  /**
   * AwsSnsController constructor.
   *
   * @param \Drupal\system\SystemManager $systemManager
   *   System Manager Service.
   * @param \Drupal\aws_sns_entity_publish\AspServices $aspServices
   *   ASP Service.
   */
  public function __construct(SystemManager $systemManager, AspServices $aspServices) {
    $this->systemManager = $systemManager;
    $this->aspServices = $aspServices;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('system.manager'),
      $container->get('aws_sns_entity_publish.asp_services')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function test() {
    $message_id = $this->aspServices->entityPublishToAwsSns('Test SNS message', 'Testing AWS SNS Entity Pusblish Message body');
    return [
      '#markup' => 'Connection successful, Test message has been sent successfully. Message id is ' . $message_id,
    ];
  }

  /**
   * Provides a aws sns publisher links page.
   */
  public function awsSnsEntityPublishPage() {
    return $this->systemManager->getBlockContents();
  }

}

**AWS SNS Entity Publish Module**
------------------

CONTENTS OF THIS FILE
---------------------

* Installation
* Requirements
* Test Connection
* Details


INTRODUCTION
------------
Use this module to Publish any type of entities to AWS SNS Topic.
Example : Entity create, update and delete.

REQUIREMENTS
------------
Require aws-php-sdk library (install this module using composer to get this
library).


INSTALLATION
------------
* Install as you would normally install a contributed Drupal module.
* Preferrably using Composer.


DETAILS
-------------
* For Configuration settings, visit
* ``Configuration > 
* AWS SNS Entity Publish``
* (``admin/config/aws-sns-entity-publish``)
* Set AWS credentials on AWS Configuration page
* (``admin/config/aws-sns-entity-publish/aws-config``)
* For testing connection if credentials added on AWS Configuration page is
* correct.
* Visit ``admin/config/aws-sns-entity-publish/test``
* Functionality : 
* We can call ``entityPublishToAwsSns`` service wherever required. 
* For Example, 
* Lets assume we need to publish on node create/update/delete,
* you can use any of these hooks
* hook_entity_insert
* hook_entity_update
* hook_entity_delete
* In this case, whenever any of those actions performed
* in the application, the notification will sent to AWS SNS
* with payload in json format.
* Notifications pushed to AWS SNS
* can be read by setting up an AWS SQS or other
* subscription service for that AWS SNS Topic.

EXAMPLE
-------------

use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_entity_update().
 */
function aws_sns_entity_publish_entity_update(EntityInterface $entity) {
  $config = \Drupal::service('config.factory')->get('aws_sns_entity_publish.awsconfig');
  $bundle_name = $entity->bundle();
  if ($bundle_name == 'YOUR_CONTENT_TYPE_NAME') {
    $asp_service = \Drupal::service('aws_sns_entity_publish.asp_services');
    // serialize the entity response.
    $payload = \Drupal::service('serializer')->serialize($entity, 'json');
    $subject = t('Updating @bundleName content.',
	['@bundleName' => $bundle_name]);
    $topic = $config->get('aws_sns_default_topic_arn');
    $message_group_id = rand();
    $message_dup_id = rand();
    $asp_service->entityPublishToAwsSns(
      $subject,
      $payload,
      $message_group_id,
      $message_dup_id,
      $topic);
  }
}
